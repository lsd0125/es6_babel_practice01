// 查看 node 支援 es6 的程度
// $ sudo npm install -g es-checker
// $ es-checker

// ------------------ part 1 建立環境 ------
// 建立專案資料夾
// 進入工作資料夾
// $ npm init
// $ code .

// $ npm install --save express

// 建立新檔 app.js

// 引入 express
var express = require('express');

// 建立 web server 物件
var app = express();

// 路由
app.get('/', function(request, response){
	response.send('Hello World!');
});

// Server 偵聽
app.listen(3000, function(){
	console.log('The app is listening on port 3000!');
});

// 測試 Server
// $ node app.js
// 使用瀏覽器查看 http://localhost:3000

// 建立資料夾 /public
// 在裡面放 a.html
// 使用靜態內容的資料夾
app.use(express.static('public'));
// 重啟 express
// 使用瀏覽器查看 http://localhost:3000/a.html

// 使用 bootstrap 的 style
// $ npm install bootstrap --save
app.use(express.static('node_modules/bootstrap/dist'));

// 在 a.html 內放入 css 和 navbar
/*
<!DOCTYPE html>
<html lang="zh_TW">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <title>Document</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            // https://getbootstrap.com/docs/4.1/components/navbar/
        </div>
    </nav>
</body>
</html>
*/

// 測試時下拉選單不會動
// 下載 jQuery 並放入 /public/js/
// html 設定來源
/*
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
*/

// 不用在修改 server 端程式後重啟 server 可使用： nodemon, supervisor, forever, pm2
// $ npm install -g nodemon
// $ nodemon app.js

// ------------------ part 1 end ------

// ------------------ part 2 使用樣板引擎 以 handlebars 為例 ------

// 安裝 handlebars - https://www.npmjs.com/package/express-handlebars
// $ npm i express-handlebars

// 建立 views 資料夾
// 設定 views 路徑 optional
// app.set('views', './views');

// 設定 views 使用的 engine
var exphbs  = require('express-handlebars');
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// 建立 template file: /views/home.handlebars
/*
	<h1>Tester: {{name}}</h1>
*/

// 建立 layouts file: /views/layouts/main.handlebars
/*
    <div class="container">
        {{{body}}}
    </div>
*/

// 修改 app.js
app.get('/', function(request, response){
    response.render('home');
});

// 如果不使用 layouts
response.render('home', {layout: false});

// 也可以加入參數
response.render('home', {
    layout: false,
    name: 'Shinder'
});

// 使用 partials (頁面內可分離、重複使用的區塊)
// 建立 partials file: /views/partials/navbar.handlebars
// 將原本在 main.handlebars 的 navbar 內容放入 navbar.handlebars
// 在 main.handlebars 原本 navbar 的位置，設定為 {{>navbar}}


// 儲存 json 檔： /data/sales.json
[
    {
        "name": "Bill",
        "age": 28,
        "id": "A001"
    },
    {
        "name": "Peter",
        "age": 32,
        "id": "A002"
    },
    {
        "name": "Carl",
        "age": 29,
        "id": "A003"
    }
]


// 滙入 json 檔的內容
app.get('/', function(request, response){
    var sales = require('./data/sales.json');
    response.render('home', {
        sales: sales
    });
});

// home.handlebars
/*
<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Age</th>
                </tr>
            </thead>
            <tbody>
                {{#each sales}}
                <tr>
                    <td>{{this.id}}</td>
                    <td>{{this.name}}</td>
                    <td>{{this.age}}</td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </div>
</div>
*/

// ------------------ part 2 end ------

// ------------------ part 3 模組及 middleware ------

// 安裝 body-parser 用來解析 http body 內容 (middleware)
// $ npm install --save body-parser
// 修改 app.js
var url = require('url');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.get('/page1', (req, res)=>{
    var urlParts = url.parse(req.url, true); // 取得 QueryString
    console.log(urlParts);
    res.render('page1a');
});
app.post('/page1', urlencodedParser,(req, res)=>{
    if (!req.body) 
        return res.sendStatus(400);
    console.log(req.body);
    res.render('page1b',{
        email: req.body.email,
        password: req.body.password
    });
});

// page1a.handlebars 內容
/*
<form method="post">
  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" name="email">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password">
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="check1" name="check1">
    <label class="form-check-label" for="check1">Check me out</label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
*/

// page1b.handlebars 內容
/*
<h1>{{email}}</h1>
<h2>{{password}}</h2>
*/

// ------ 以下為參考內容 ------
// 回應 json 格式的文件
response.json(object);

// router 變數代稱
app.get('/admin/rooms/delete/:id', (request, response)=>{
	var roomId = request.params.id;

	// 保留 Id 不同者
	rooms = rooms.filter(r => r.id !== roomId);
	response.redirect('admin/rooms');
});

// page not found!
response.sendStatus(404);

// *** 把一些功能分離開來變成模組 1 *******
// 例如： admin.js
module.exports = function(app) {
	// ...
};
// 在 app.js
var admin = require("./admin");
admin(app);

// *** 把一些功能分離開來變成模組 2 *******
// 在 admin.js 中使用 router
var router = express.Router();
module.exports = router;
router.get('admin/rooms', function(request, response){
	// ...
});
// 在 app.js
var adminRouter = require("./admin");
app.use(adminRouter); //當成 middleware 使用

// *** 把一些功能分離開來變成模組 3 *******
// 方式如 2, 路徑做變更
app.use('/admin', adminRouter); // module 裡面的路徑用相對路徑
// 在 admin.js 裡
response.redirect(request.baseUrl + '/rooms');
response.redirect('./'); // 也可以使用相對路徑

// 鏈狀語法
router.route('/rooms/add')
	.get((req, res)=>{
	})
	.post((req, res)=>{
	});

//next 用法
app.use(function(request, response, next){
	console.log(`Incoming request: ${request.url}`);
	next();
});
//next 用法
router.route('/rooms/edit/:id')
	.all(function(request, response, next){
		var roomId = request.params.id;
		var room = _.find(rooms, r=>r.id===roomId);
		if(!room){
			response.sendStatus(404);
			return;
		}
		response.locals.room = room;
		next();
	})
	.get(function(request, response){
		response.render('edit');
	})
	.post(function(request, response){
		// ...
	});

// ------------------ part 3 end ------

// ------------------ part 4 MySQL 連線 ------
// 在 test 資料庫設定資料
/*
CREATE TABLE IF NOT EXISTS `sales` (
  `sid` int(11) NOT NULL,
  `sales_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `sales` (`sid`, `sales_id`, `name`, `birthday`) VALUES
(1, 'A001', '李小明', '1990-09-13'),
(2, 'B002', '陳小華', '1989-01-10'),
(3, 'A002', '吳小安', '1990-02-20');

ALTER TABLE `sales`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `sales_id` (`sales_id`);
ALTER TABLE `sales`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
*/

// 安裝 https://www.npmjs.com/package/mysql
// $ npm install mysql

// 修改 app.js
var mysql = require('mysql');
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'test'
});
db.connect();
app.get('/page2', (req, res)=>{
    var sql = "SELECT * FROM `sales`";
    db.query(sql, function(error, results, fields){
        if(error) throw error;
        console.log(results);
        res.render('page2', {
            sales: results
        });
    });
});

// 建立 /views/page2.handlebars
/*
<div class="row">
    <div class="col-md-6">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Birthday</th>
                </tr>
            </thead>
            <tbody>
                {{#each sales}}
                <tr>
                    <td>{{this.sales_id}}</td>
                    <td>{{this.name}}</td>
                    <td>{{this.birthday}}</td>
                </tr>
                {{/each}}
            </tbody>
        </table>
    </div>
</div>
*/
// 查看 http://localhost:3000/page2

// 日期格式問題
// 使用 moment
// $ npm install moment
// 修改 app.js
var moment = require('moment');
app.get('/page2', (req, res)=>{
    var sql = "SELECT * FROM `sales`";
    db.query(sql, function(error, results, fields){
        if(error) throw error;
        results.forEach(el => {
            el.birthday = moment(el.birthday).format('YYYY-MM-DD');
        });
        res.render('page2', {
            sales: results
        });
    });
});

// ------------------ part 4 end ------





