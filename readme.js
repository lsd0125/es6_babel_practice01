// 建立專案資料夾
// $ mkdir my_babel_01
// $ cd my_babel_01
// $ npm init #使用預設值

// 安裝 babel
// $ npm install --save-dev @babel/core @babel/cli @babel/preset-env

// 安裝 polyfill
// $ npm install --save-dev @babel/polyfill

// 建立 babel 設定檔並設定內容
// $ touch .babelrc
/*
{ "presets": ["@babel/preset-env"] }
*/
// 建立 src 和 dist 資料夾

// 查看 babel 參數
// $ ./node_modules/.bin/babel --help

// 在 package.json 檔裡的 scripts 設定
// "transpile": "./node_modules/.bin/babel src --out-dir dist"

// 在 src 裡新增檔案 app.js
let f = name=>{
    console.log(`hello ${name}`);
};
f('bill');

// $ npm run transpile 
// 轉譯後，在 dist 可以看到轉成 es5 語法的 app.js

// 在 src 內建立 person.js
class Person {
    constructor(firstname, lastname, age=20, gender='male') {
        this.firstname = firstname;
        this.lastname =  lastname;
        this.age =  age;
        this.gender =  "male";
    }

    toString() {
        return this.firstname + ' ' + this.lastname;
    }

    describe() {
        return `${this.firstname} ${this.lastname}
            age: ${this.age}
            gender: ${this.gender}`;
    }
}
export default Person;

// 在 app.js 裡使用
import Person from './person';
let p = new Person('Peter', 'Lin');
console.log(p.toString());
console.log(p.describe());

// 初步環境完成 *************

// ************** 使用 webpack **************
// 查看版本
// $ npm view webpack version

// 本地安裝 webpack
// $ npm install webpack --save-dev
// $ npm install -D webpack-cli

// 建立 webpack.config.js
const path = require('path');
module.exports = {
    entry: './dist/app.js',
    mode: 'development', // development or production
    output: {
      path: path.resolve(__dirname, 'build'),
      filename: 'bundle.js'
    }
};

// 在 package.json 檔裡的 scripts 設定
// "webpack": "./node_modules/.bin/webpack"

// 執行 webpack
// $ npm run webpack

// 新增 index.html
/*
<body>
    <div id="app"></div>
    <script src="./build/bundle.js"></script>
</body>
*/

// 修改 app.js
import Person from './person';
const app = document.querySelector('#app');
let persons = [
    new Person('Peter', 'Lin', 26, 'male'),
    new Person('Bill', 'Chen', 28, 'male'),
    new Person('Flora', 'Hsu', 25, 'female'),
];
persons = [...persons, new Person('小明', '李')];

persons.forEach(p=>{
    app.innerHTML += `<div data-age="${p.age}">${p.firstname} ${p.lastname}</div>`;
});

// $ npm run transpile
// $ npm run webpack

// ********** 使用 babel-loader (webpack 外掛) **********
// 安裝 babel-loader
// $ npm i -D babel-loader

// 修改 webpack.config.js 設定 entry: './src/app.js'
// 刪除 dist 資料夾 (用不到了)
module: {
  rules: [
    {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }
    }
  ]
}







