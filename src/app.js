// import my_map from './my_map';
// import my_proxy from './my_proxy';
// import my_destructuring_assignment from './my_destructuring_assignment';
// import my_promise from './my_promise';
// import my_object_literals from './my_object_literals';
// import my_fetch from './my_fetch';

import "@babel/polyfill";

let app, divShow;
try {
    app = document.querySelector('#app');
    divShow = s => {
        app.innerHTML += s + '<br>';
    }
} catch(error) {
    divShow = console.log;
}

async function what(url){
    let a = await fetch(url);
    divShow('1:');
    divShow(a);
    return a.text();
}

async function what2(url) {
    let txt = await what(url);
    divShow('2:');
    divShow(txt);
    return txt;
}

what2('./src/person.js');

divShow('end');

/*
順序：
    0: module, export
    1: let, const
    2: Map, Set
    3: Destructuring assignment
    4: Object literals
    5: Proxy
    6: Promise
    7: fetch()
    8: OOP
        constructor
        properties
        methods
        setter & getter
        extends

*/

