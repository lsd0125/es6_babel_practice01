const my_map = divShow => {
    const map = new Map;
    const obj = {};

    map.set({}, {a:1, b:2});
    map.set({}, {a:3, b:4});
    map.set(obj, {a:5, b:6});
    map.set(obj, {a:7, b:8});
    map.forEach((v,k)=>{
        divShow( JSON.stringify(k) + ' ::: ' + JSON.stringify(v) );
    });
    divShow( map.size );
    divShow( '---' );

    const set = new Set;

    set.add({a:1, b:2}).add({a:1, b:2});
    set.add(obj).add(obj).add({a:1, b:2});
    set.forEach(v=>{
        divShow(JSON.stringify(v));
    });
    divShow( set.size );
    set.delete(obj);
    divShow( set.size );
    set.clear();
    divShow( set.size );
};

export default my_map;