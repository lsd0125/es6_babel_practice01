const my_proxy = divShow => {
    const target = {
        name: 'bill',
        age: 24,
        title: 'engineer'
    };
    const handle = {
        get: (target, prop) => {
            return (prop in target) ? target[prop] : 'No this property!';
        },
        set: (target, prop, value) => {
            if(prop==='age'){
                if(isNaN(parseFloat(value))){
                    throw new ReferenceError('The age must be numerical!');
                    return false;
                }
            }
            target[prop] = value;
            return true;
        }
    };
    const proxy = new Proxy(target, handle);

    divShow('name: ' + proxy.name);
    divShow('gender: ' + proxy.gender);
    proxy.age = 30;
    divShow( JSON.stringify(proxy) );
    try{
        proxy.age = 'peter';
    }catch(error){
        divShow( error );
    }
    
    divShow( JSON.stringify(proxy) );
};


export default my_proxy;