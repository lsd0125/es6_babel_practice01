const my_promise = divShow => {
    divShow(':0');
    const promise = new Promise((resolve, reject)=>{
        divShow(':1');
        const t = Math.floor(Math.random()*500) + 500;
        setTimeout(()=>{
            if(t>700){
                resolve(t);
            } else {
                reject(new Error('My error! '+t));
            }
            divShow(':4');
        }, t);
        divShow(':2');
    });

    divShow(':3');
    promise.then(v=>{
        divShow(v);
        divShow(':5');
    }, error=>{
        divShow(error);
        divShow(':5-');
    });

    new Promise((resolve, reject)=>{
        const t = Math.floor(Math.random()*500) + 500;
        setTimeout(()=>{
            resolve(t);
        }, t);
    }).then(v=>{
        divShow('Hello Promise: '+v);
    });    
};

export default my_promise;