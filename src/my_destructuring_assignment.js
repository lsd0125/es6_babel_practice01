const my_destructuring_assignment = divShow => {
    const data = {
        name: 'david',
        age: 28,
        gender: 'male'
    };

    let {name, gender, age} = data;
    const {a, b, c} = data;

    divShow(name +' '+ gender +' '+ age);
    divShow(a +' '+ b +' '+ c);

    data.age ++;
    ({name, gender, age} = data);
    divShow(name +' '+ gender +' '+ age);

    /*
    // 目前還是草案
    // @babel/plugin-proposal-object-rest-spread
    let data2 = {
        ...data, 
        title: 'developer'
    };
    divShow(JSON.stringify(data2));
    */

    const ar = [3, 5, 8];
    let [d, e, f] = ar;
    const [g, h] = ar;
    divShow(d +' '+ e +' '+ f);
    divShow(g +' '+ h);

    [d, e, f] = [9, 99, 999];
    divShow(d +' '+ e +' '+ f);

    const br = [2, ...ar, 9];
    divShow(br.toString());
};

export default my_destructuring_assignment;