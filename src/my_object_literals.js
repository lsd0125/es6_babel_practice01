const my_object_literals = divShow => {
    let [a, b, c] = [6, 7, 8];
    const obj1 = {a, b, c};

    divShow(JSON.stringify(obj1));
    
    const obj2 = {
        hello(who='Peter'){
            return `Hello ${who}`;
        },
        sum(){
            const ar = [...arguments];
            if(ar.length>1){
                return ar.shift() + this.sum(...ar);
            } else {
                return ar[0];
            }
        }
    }
    divShow( obj2.hello('John') );
    divShow( obj2.sum(7, 5, 6) );

    const obj3 = {
        hello: who=>{
            return `Hello ${who}`;
        },
        sum: (...arg)=>{  // no "arguments"
            let s=0, i=0;
            for(; i<arg.length; s+=arg[i], i++);
            return s;
        }
    }
    divShow( obj3.hello('Flora') );
    divShow( obj3.sum(3, 9, 8) );

};

export default my_object_literals;