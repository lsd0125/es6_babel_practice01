// https://developer.mozilla.org/zh-TW/docs/Web/API/Fetch_API/Using_Fetch
const my_fetch = divShow => {

    fetch('./src/app.js')
        .then(response=>{
            return response.text();
        })
        .then(txt=>{
            divShow('<pre>' + txt + '</pre>');
        });

    fetch('./src/person.js')
        .then(response=>{
            if(!response.ok){
                console.log(response);
                throw new Error(response.status + ' ' + response.statusText);
            }
            return response.text();
        })
        .then(txt=>{
            divShow('<pre>' + txt + '</pre>');
        })
        .catch(error=>{
            divShow(error);
        });
};

export default my_fetch;